

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MyNews
// ====================================================

export interface MyNews_allNews_edges_node {
  id: string;  // The ID of the object.
  title: string;
  createdDate: any;
  slug: string | null;
}

export interface MyNews_allNews_edges {
  node: MyNews_allNews_edges_node | null;  // The item at the end of the edge
}

export interface MyNews_allNews_pageInfo {
  startCursor: string | null;  // When paginating backwards, the cursor to continue.
  endCursor: string | null;    // When paginating forwards, the cursor to continue.
  hasNextPage: boolean;        // When paginating forwards, are there more items?
}

export interface MyNews_allNews {
  edges: (MyNews_allNews_edges | null)[];
  pageInfo: MyNews_allNews_pageInfo;
}

export interface MyNews {
  allNews: MyNews_allNews | null;
}

/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================