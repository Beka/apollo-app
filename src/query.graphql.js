import { gql } from "apollo-boost";

const myQuery = gql`
  query MyNews($cursor: String) {
    allNews(first: 5, after: $cursor) {
      edges {
        node {
          id
          title
          createdDate
          slug
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;

export { myQuery };
