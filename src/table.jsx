import React, { PureComponent, Fragment } from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";

export class BananaTable extends PureComponent {
  render() {
    const mapData = () => {
      const { allNews } = this.props;

      console.log("Data inside component: ", allNews);

      if (allNews && allNews.edges && allNews.edges.length > 0) {
        return allNews.edges.map(item => ({
          id: item.node.id,
          title: item.node.title,
          createdDate: item.node.createdDate,
          slug: item.node.slug
        }));
      }
    };

    return (
      <Fragment>
        <Typography variant="h3" gutterBottom>
          Ethiopian Movie Database
        </Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Title</TableCell>
                <TableCell>Date created</TableCell>
                <TableCell>Slug</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {mapData().map(item => (
                <TableRow key={item.id}>
                  <TableCell>{item.title}</TableCell>
                  <TableCell>{item.createdDate}</TableCell>
                  <TableCell>{item.slug}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <br />
        <Grid container justify="center">
          <Button
            variant="contained"
            color="primary"
            onClick={this.props.onLoadMore}
          >
            Load more...
          </Button>
        </Grid>
      </Fragment>
    );
  }
}
