import React, { Component } from "react";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink
} from "apollo-boost";
import { ApolloProvider, Query } from "react-apollo";

import { BananaTable } from "./table";

import { myQuery } from "./query.graphql";

const Client = new ApolloClient({
  link: ApolloLink.from([
    new HttpLink({
      uri: "https://etmdb.com/graphql"
    })
  ]),
  cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={Client}>
        <Query query={myQuery}>
          {({ loading, error, data: { allNews }, fetchMore }) => {
            if (loading) {
              return <p>Loading...</p>;
            }
            if (error) {
              return <p>{`Error... ${error}`}</p>;
            }

            console.log(allNews);

            const loadMore = () =>
              fetchMore({
                variables: {
                  cursor: allNews.pageInfo.endCursor
                },
                updateQuery: (previousResult, { fetchMoreResult }) => {
                  const newEdges = fetchMoreResult.allNews.edges;
                  const pageInfo = fetchMoreResult.allNews.pageInfo;

                  return newEdges.length > 0
                    ? {
                        allNews: {
                          __typename: previousResult.allNews.__typename,
                          edges: [...previousResult.allNews.edges, ...newEdges],
                          pageInfo
                        }
                      }
                    : previousResult;
                }
              });

            return (
              !loading &&
              !error && <BananaTable allNews={allNews} onLoadMore={loadMore} />
            );
          }}
        </Query>
      </ApolloProvider>
    );
  }
}

export default App;
